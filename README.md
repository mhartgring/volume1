# Ansible playbook to share /volume1 on Synology over Samba/CIFS

If you found this, kudos!

This is a playbook you can run against your Synology nas to share the /volume1 directory.

The rights of user mounting the share will be reflected.
If you are an admin you will be able to access everything.
If you are a limited user you will still see everything but you will only be able to access those shares you have been granted access to.

## Caveats
- ssh must be enabled
- you must be an admin, else you cannot become root
- DMS7 and up, there's an instruction for running against DMS6 in the playbook

## Howto
- edit the hosts file to reflect your environment
- only change the IP and the ansible_user
- run ```ansible-playbook -K -k -i hosts playbook.yml```

### DISCLAIMER

**Don't bother asking for help with ansible, find someone who does or follow [this](https://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html) link for help.**

***I am NOT responsible for mistakes you make, you're on your own if you do this.***
